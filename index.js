const bodyParser = require("body-parser");
const express = require('express')
const mongoose = require('mongoose');
const path = require('path');
const rp = require('request-promise');
const app = express();
require('dotenv').config();

//connect to db
mongoose.connect('mongodb://localhost/test');
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("connected!");
});

const tripSchema = {
  origin: String,
  destination: String,
  duration: Number,
  distance: Number
}

const Trip = mongoose.model('Trip', tripSchema);

const staticPath = path.join(__dirname, 'public')

app.use('/static', express.static(staticPath))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) => res.sendFile(path.join(staticPath, "index.html")));

app.get('/trip', function (req, res) {
  Trip.find().then((trips) => {
    res.json(trips);
  }); 
});

const distanceUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?";
const googleApiKey = process.env.GOOGLE_API_KEY;

app.post('/trip', function (req, res, next) {
  const options = { json: true };
  const url = distanceUrl
    + "origins=" + req.body.origin
    + "&destinations=" + req.body.destination
    + "&key=" + googleApiKey;
  
  origin = rp(url, options).then((response) => {
    if (response.rows[0].elements[0].status === "NOT_FOUND")
      throw new Error("No results");
    else {
      return new Trip({
        origin: response.origin_addresses[0],
        destination: response.destination_addresses[0],
        distance: response.rows[0].elements[0].distance.value,
        duration: response.rows[0].elements[0].duration.value,
      }).save();
    }
  }).catch((err) => {
    console.log("ERROR", err);
  })
  .then((trip) => {
    return Trip.find();
  }).then((trips) => {
    res.json(trips);
  });
});

app.listen(4000, () => console.log('Example app listening on port 4000!'))
