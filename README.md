###Tech
* MongoDB  
* Mongoose  
* Express  


###API methods  
* GET trips  
* POST trip


This application requires MongoDB and NodeJS.

Run "npm install"

Add a .env file with a value for GOOGLE_API_KEY

Run "node server.js"

Go to localhost:4000
