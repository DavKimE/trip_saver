onInit();

function onInit() {
  request("GET", "/trip", updateTable);
}

function save() {
  const tripForm = document.getElementById("tripForm");
  const tripJSON = generateJSON(tripForm);

  request("POST", "/trip", updateTable, tripJSON);
}

function generateJSON(node) {
  const inputs = node.getElementsByTagName("input");
  const tripJSON = {};
  for (var i = 0; i < inputs.length; i++) {
    let input = inputs[i];
    tripJSON[input.name] = input.value;
  }
  return tripJSON;
}


function clearTable() {
  const table = document.getElementById("table");
  const tableBody = document.getElementsByTagName("tbody")[0];
  const newTableBody = document.createElement("tbody");

  table.replaceChild(newTableBody, tableBody);
}

function generateTable(data) {
  const tableBody = document.getElementsByTagName("tbody")[0];
  var row, trip;

  for (let i = 0; i < data.length; i++) {
    row = tableBody.insertRow(0);
    trip = data[i];

    row.insertCell(0).innerHTML = trip.origin;
    row.insertCell(1).innerHTML = trip.destination;
    row.insertCell(2).innerHTML = trip.duration;
    row.insertCell(3).innerHTML = trip.distance;
  }
}

function updateTable() {
  clearTable();
  generateTable(JSON.parse(this.responseText));
}

function request(type, url, callback, data) {
  var xhr = new XMLHttpRequest();

  xhr.open(type, url, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(data));

  xhr.onload = callback;
}
